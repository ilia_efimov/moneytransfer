# MoneyTransfer

This is example of money transfer REST service implementation. 

It can be builded from sources using maven or downloaded from Downloads tab.

For launching builded service you have to just go to bin folder and run run_service.bat file. After initialization you'll see something like this: 
2018-06-26 10:31:21,903 INFO org.eclips.jetty.server.Server [Thread-3] Started @21725ms

This service is started with preloaded data from accounts.csv file.
After starting the service you can watch data which have been downloaded. For doing this just run view_all_data.bat. After several seconds you'll see data in memory db.

REST API for using:
patch: http://localhost:8351/rest/transfer  (do one transfering between accounts)
body example:
{
	"from": "40701810165780000012",
	"to": "40701810165780000009",
	"amount": 1000000.00
}


patch: http://localhost:8351/rest/bulktransfer  (do bulk transfering between accounts)
body example:
[
	{
		"from": "40701810165780000012",
		"to": "40701810165780000009",
		"amount": 1000000.00
	},

	{
		"from": "40701810165780000012",
		"to": "40701810165780000010",
		"amount": 1000000.00
	}
]

