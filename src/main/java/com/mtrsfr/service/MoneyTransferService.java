package com.mtrsfr.service;

import com.mtrsfr.dto.MoneyTransferInfo;
import com.mtrsfr.exceptions.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class offer methods for money transferring
 */
public abstract class MoneyTransferService {

    protected static Logger logger = LogManager.getLogger(MoneyTransferService.class);

    /**
     * This method must be implemented for specific DB
     * Do money transferring between accounts
     * @param transferInfo Information about accounts and amount
     */
    public abstract void doTransfer(MoneyTransferInfo transferInfo);

    /**
     * This method do bulk money transferring for many accounts
     * @param transfersInfo List of information about accounts and amount
     * @return
     */
    public Map<String, String> doBulkTransfer(List<MoneyTransferInfo> transfersInfo) {
        Map<String, String> errors = new HashMap<>();
        for (MoneyTransferInfo transferInfo : transfersInfo) {
            try {
                this.doTransfer(transferInfo);
            } catch (ServiceException e) {
                errors.put(String.format("%s => %s", transferInfo.getFrom(), transferInfo.getTo()), e.getMessage());
            }
        }
        return errors;
    }
}
