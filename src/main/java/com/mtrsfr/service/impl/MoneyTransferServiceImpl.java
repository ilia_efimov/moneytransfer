package com.mtrsfr.service.impl;

import com.mtrsfr.cache.Account;
import com.mtrsfr.dto.MoneyTransferInfo;
import com.mtrsfr.exceptions.ServiceException;
import com.mtrsfr.service.MoneyTransferService;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.transactions.Transaction;

import static org.apache.ignite.transactions.TransactionConcurrency.PESSIMISTIC;
import static org.apache.ignite.transactions.TransactionIsolation.REPEATABLE_READ;

/**
 * Implementation for ignite in-memory kv storage
 */
public class MoneyTransferServiceImpl extends MoneyTransferService {

    private final Ignite ignite;

    private static final String CACHE_NAME = "account";

    public MoneyTransferServiceImpl(Ignite ignite) {
        this.ignite = ignite;
    }

    @Override
    public void doTransfer(MoneyTransferInfo transferInfo) {
        IgniteCache<String, Account> accCache = ignite.getOrCreateCache(CACHE_NAME);
        try (Transaction transaction = ignite.transactions().txStart(PESSIMISTIC, REPEATABLE_READ)) {
            Account accountFrom = accCache.get(transferInfo.getFrom());
            Account accountTo = accCache.get(transferInfo.getTo());

            try {
                if (accountFrom.getAmount().compareTo(transferInfo.getAmount()) < 0) {
                    throw new ServiceException("Insufficient funds on account");
                }

                accountFrom = new Account(accountFrom.getAccountNumber(),
                        accountFrom.getAmount().subtract(transferInfo.getAmount()));

                accountTo = new Account(accountTo.getAccountNumber(), accountTo.getAmount().add(transferInfo.getAmount()));

                accCache.put(transferInfo.getFrom(), accountFrom);
                accCache.put(transferInfo.getTo(), accountTo);

                transaction.commit();
            } catch (Exception e) {
                logger.error(e);
                transaction.rollback();
                throw new ServiceException(e.getMessage(), e);
            }
        }
    }
}
