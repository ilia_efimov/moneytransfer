package com.mtrsfr.exceptions;

/**
 * Custom exception class which cover all services errors.
 * Can be also expanded for example for supporting localization
 */
public class ServiceException extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable ex) {
        super(message, ex);
    }

}
