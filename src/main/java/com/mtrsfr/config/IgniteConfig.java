package com.mtrsfr.config;

import com.google.common.collect.ImmutableList;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.configuration.TransactionConfiguration;
import org.apache.ignite.spi.communication.CommunicationSpi;
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;

import java.util.List;

/**
 * Configuration kv storage Apache Ignite
 */
public class IgniteConfig {

    /**
     * Do configuration and return ignite object
     * @return ignite. Object for working with kv storage Apache Ignite
     */
    public Ignite getIgnite() {
        IgniteConfiguration igniteConf = new IgniteConfiguration();
        igniteConf.setDiscoverySpi(getTcpDiscoverySpi(ImmutableList.of("localhost")));
        igniteConf.setLocalHost("localhost");

        igniteConf.setCommunicationSpi(getCommunicationSpi("localhost"));
        igniteConf.setTransactionConfiguration(getTransactionConfiguration());

        return Ignition.start(igniteConf);
    }

    private static TcpDiscoverySpi getTcpDiscoverySpi(final List<String> ipAddrs) {
        TcpDiscoveryVmIpFinder tcpDiscoveryVmIpFinder = new TcpDiscoveryMulticastIpFinder();
        tcpDiscoveryVmIpFinder.setAddresses(ipAddrs);
        TcpDiscoverySpi tcpDiscoverySpi = new TcpDiscoverySpi();
        tcpDiscoverySpi.setIpFinder(tcpDiscoveryVmIpFinder);
        return tcpDiscoverySpi;
    }

    private static CommunicationSpi getCommunicationSpi(String ipAddr) {
        TcpCommunicationSpi communicationSpi = new TcpCommunicationSpi();
        communicationSpi.setLocalAddress(ipAddr);
        return communicationSpi;
    }

    private static TransactionConfiguration getTransactionConfiguration() {
        TransactionConfiguration configuration = new TransactionConfiguration();
        configuration.setDefaultTxTimeout(600000);
        configuration.setDefaultTxIsolation(TransactionIsolation.REPEATABLE_READ);
        configuration.setDefaultTxConcurrency(TransactionConcurrency.PESSIMISTIC);
        return configuration;
    }
}
