package com.mtrsfr.config;

import com.google.gson.Gson;
import com.mtrsfr.controller.MoneyTransferRestController;
import com.mtrsfr.dto.ErrorDto;
import com.mtrsfr.exceptions.ServiceException;
import com.mtrsfr.service.MoneyTransferService;

import static spark.Spark.*;

public class ControllerConfig {

    private static final int STATUS_ERROR = 404;

    private final MoneyTransferRestController controller;

    /**
     * Do controller configuration
     * @param service service instance
     */
    public ControllerConfig(MoneyTransferService service) {
        controller = new MoneyTransferRestController(service);
    }

    public void config() {
        port(8351);
        patch("/rest/transfer", controller::transfer);
        patch("/rest/bulktransfer", controller::bulkTransfer);
        exception(ServiceException.class, (exception, request, response) -> {
            response.status(STATUS_ERROR);
            response.type("text/json");
            response.body(new Gson().toJson(new ErrorDto(exception.getMessage())));
        });
        internalServerError((req, res) -> {
            res.type("application/json");
            return new Gson().toJson(new ErrorDto("Bad request format"));
        });
    }
}
