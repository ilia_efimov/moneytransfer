package com.mtrsfr.cache;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Object (entity) for transferring between DB and service
 */
public class Account implements Serializable {
    private final String accountNumber;
    private final BigDecimal amount;

    public Account(String accountNumber, BigDecimal amount) {
        this.accountNumber = accountNumber;
        this.amount = amount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", accountNumber, amount);
    }
}
