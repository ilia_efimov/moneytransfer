package com.mtrsfr.controller;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mtrsfr.dto.MoneyTransferInfo;
import com.mtrsfr.exceptions.ServiceException;
import com.mtrsfr.service.MoneyTransferService;
import org.apache.commons.lang3.StringUtils;
import spark.Request;
import spark.Response;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class MoneyTransferRestController {

    private static final int STATUS_OK = 200;

    private final MoneyTransferService moneyTransferService;

    public MoneyTransferRestController(MoneyTransferService service) {
        this.moneyTransferService = service;
    }

    /**
     * Do money transferring between accounts
     * @param request contains body with information in json format
     * @param response contains only status or error message
     * @return
     */
    public String transfer(Request request, Response response) {
        try {
            String json = request.body();
            Gson gson = new Gson();
            MoneyTransferInfo transferInfo = gson.fromJson(json, MoneyTransferInfo.class);
            moneyTransferService.doTransfer(transferInfo);
            response.status(STATUS_OK);
        } catch (ServiceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Do bulk money transferring for many accounts
     * @param request contains body with information in json format
     * @param response contains only status or error message
     * @return
     */
    public String bulkTransfer(Request request, Response response) {
        String json = request.body();
        if (json == null) {
            throw new ServiceException("Request body is wrong!");
        }
        try {
            Type transfersType = new TypeToken<ArrayList<MoneyTransferInfo>>() {
            }.getType();
            List<MoneyTransferInfo> transfersInfo = new Gson().fromJson(json, transfersType);
            Map<String, String> errors = moneyTransferService.doBulkTransfer(transfersInfo);
            if (errors.size() > 0) {
                String errJson = new Gson().toJson(errors);
                throw new ServiceException(errJson);
            }
            response.status(STATUS_OK);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return StringUtils.EMPTY;
    }
}
