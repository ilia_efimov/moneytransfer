package com.mtrsfr;

import com.mtrsfr.config.ControllerConfig;
import com.mtrsfr.config.IgniteConfig;
import com.mtrsfr.cache.Account;
import com.mtrsfr.service.MoneyTransferService;
import com.mtrsfr.service.impl.MoneyTransferServiceImpl;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.ScanQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * This is main class which does all configurations
 */
public class App {

    private static Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        Ignite ignite = new IgniteConfig().getIgnite();

        if (args.length == 0 || args[0].equals("--fillData")) {
            MoneyTransferService service = new MoneyTransferServiceImpl(ignite);
            new ControllerConfig(service).config();

            if (args.length > 0 && args[0].equals("--fillData")) {
                if (args.length == 1) {
                    logger.info("Usage: --fillData <path to csv file>");
                    System.exit(-1);
                } else {
                    try {
                        fillData(ignite, args[1]);
                    } catch (IOException e) {
                        logger.error(e);
                        System.exit(-1);
                    }
                }
            }
        }

        if (args.length > 0 && args[0].equals("--printData")) {
            printCacheData(ignite);
            System.exit(0);
        }
    }

    private static void fillData(Ignite ignite, String csvFileName) throws IOException {
        if (!Files.exists(Paths.get(csvFileName))) {
            throw new FileNotFoundException();
        }

        Reader in = new FileReader(csvFileName);
        Iterable<CSVRecord> records = CSVFormat.newFormat(';').parse(in);

        IgniteCache<String, Account> cache = ignite.getOrCreateCache("account");

        for (CSVRecord record : records) {
            String accNumber = record.get(0);
            BigDecimal amount = new BigDecimal(record.get(1));
            cache.put(accNumber, new Account(accNumber, amount));
        }
    }

    private static void printCacheData(Ignite ignite) {
        IgniteCache<String, Account> cache = ignite.getOrCreateCache("account");

        List<Account> accounts = new ArrayList<>();
        cache.query(new ScanQuery<>(null)).forEach(e -> accounts.add((Account) e.getValue()));
        System.out.println("Data:");
        accounts.forEach(System.out::println);
    }
}
