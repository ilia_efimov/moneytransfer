package com.mtrsfr.dto;

import java.io.Serializable;

/**
 * Data transfer object for converting errors to json and sending to client
 */
public class ErrorDto implements Serializable {
    private final String errorMessage;

    public ErrorDto(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    public String getErrorMessage() {
        return errorMessage;
    }
}
