package com.mtrsfr;

import com.google.common.collect.ImmutableList;
import com.mtrsfr.cache.Account;
import com.mtrsfr.dto.MoneyTransferInfo;
import com.mtrsfr.exceptions.ServiceException;
import com.mtrsfr.service.MoneyTransferService;
import com.mtrsfr.service.impl.MoneyTransferServiceImpl;
import org.apache.ignite.IgniteCache;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 *
 */
public class ServicesTest extends BaseTestClass {

    @Test
    public void moneyTransferTest() {
        IgniteCache<String, Account> accountCache = ignite.getOrCreateCache(CACHE_NAME);
        accountCache.put("1", new Account("1", new BigDecimal("750000.10")));
        accountCache.put("2", new Account("2", new BigDecimal("37000.84")));

        MoneyTransferService service = new MoneyTransferServiceImpl(ignite);

        service.doTransfer(new MoneyTransferInfo("1", "2", new BigDecimal("5000.50")));
        Assert.assertEquals(accountCache.get("1").getAmount(), new BigDecimal("744999.60"));
        Assert.assertEquals(accountCache.get("2").getAmount(), new BigDecimal("42001.34"));
    }

    @Test
    public void bulkMoneyTransferTest() {
        IgniteCache<String, Account> accountCache = ignite.getOrCreateCache(CACHE_NAME);

        accountCache.put("1", new Account("1", new BigDecimal("10000000.00")));
        accountCache.put("2", new Account("2", BigDecimal.valueOf(0)));
        accountCache.put("3", new Account("3", BigDecimal.valueOf(0)));
        accountCache.put("4", new Account("4", BigDecimal.valueOf(0)));
        accountCache.put("5", new Account("5", BigDecimal.valueOf(0)));
        accountCache.put("6", new Account("6", BigDecimal.valueOf(0)));

        MoneyTransferService service = new MoneyTransferServiceImpl(ignite);

        List<MoneyTransferInfo> accounts = ImmutableList.of(
                new MoneyTransferInfo("1", "2", new BigDecimal("250000.50")),
                new MoneyTransferInfo("1", "3", new BigDecimal("240000.50")),
                new MoneyTransferInfo("1", "4", new BigDecimal("180100.30")),
                new MoneyTransferInfo("1", "5", new BigDecimal("480000.28")),
                new MoneyTransferInfo("1", "6", new BigDecimal("120000.50"))
                );

        service.doBulkTransfer(accounts);

        assertEquals(accountCache.get("1").getAmount(), new BigDecimal("8729897.92"));
        assertEquals(accountCache.get("2").getAmount(), new BigDecimal("250000.50"));
        assertEquals(accountCache.get("3").getAmount(), new BigDecimal("240000.50"));
        assertEquals(accountCache.get("4").getAmount(), new BigDecimal("180100.30"));
        assertEquals(accountCache.get("5").getAmount(), new BigDecimal("480000.28"));
        assertEquals(accountCache.get("6").getAmount(), new BigDecimal("120000.50"));
    }

    @Test
    public void insufficientFundsTest() {
        IgniteCache<String, Account> accountCache = ignite.getOrCreateCache(CACHE_NAME);
        accountCache.put("1", new Account("1", new BigDecimal("3.00")));
        accountCache.put("2", new Account("2", new BigDecimal("3.00")));

        try {
            new MoneyTransferServiceImpl(ignite).doTransfer(new MoneyTransferInfo("1", "2", new BigDecimal("3.01")));
        } catch (ServiceException e) {
            return;
        }
        fail();
    }
}
