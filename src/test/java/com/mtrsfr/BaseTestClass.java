package com.mtrsfr;

import com.google.common.collect.ImmutableList;
import com.mtrsfr.cache.Account;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.configuration.TransactionConfiguration;
import org.apache.ignite.spi.communication.CommunicationSpi;
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;
import org.junit.After;
import org.junit.Before;

import java.util.List;

/**
 * Do ignite initialization
 */
public class BaseTestClass {

    static final String CACHE_NAME = "account";
    Ignite ignite;

    @Before
    public void init() {
        this.ignite = igniteConfiguration();
    }

    @After
    public void destroy() {
        this.ignite.close();
    }

    private Ignite igniteConfiguration() {
        IgniteConfiguration igniteConf = new IgniteConfiguration();
        igniteConf.setDiscoverySpi(getTcpDiscoverySpi(ImmutableList.of("localhost")));
        igniteConf.setLocalHost("localhost");

        igniteConf.setCommunicationSpi(getCommunicationSpi("localhost"));
        igniteConf.setTransactionConfiguration(getTransactionConfiguration());

        CacheConfiguration<String, Account> cacheConfiguration = new CacheConfiguration<>(CACHE_NAME);
        igniteConf.setCacheConfiguration(cacheConfiguration);

        return Ignition.start(igniteConf);
    }

    private static TcpDiscoverySpi getTcpDiscoverySpi(final List<String> ipAddrs) {
        TcpDiscoveryVmIpFinder tcpDiscoveryVmIpFinder = new TcpDiscoveryVmIpFinder();
        tcpDiscoveryVmIpFinder.setAddresses(ipAddrs);
        TcpDiscoverySpi tcpDiscoverySpi = new TcpDiscoverySpi();
        tcpDiscoverySpi.setIpFinder(tcpDiscoveryVmIpFinder);
        return tcpDiscoverySpi;
    }

    private static CommunicationSpi getCommunicationSpi(String ipAddr) {
        TcpCommunicationSpi communicationSpi = new TcpCommunicationSpi();
        communicationSpi.setLocalAddress(ipAddr);
        return communicationSpi;
    }

    private static TransactionConfiguration getTransactionConfiguration() {
        TransactionConfiguration configuration = new TransactionConfiguration();
        configuration.setDefaultTxTimeout(600000);
        configuration.setDefaultTxIsolation(TransactionIsolation.READ_COMMITTED);
        configuration.setDefaultTxConcurrency(TransactionConcurrency.OPTIMISTIC);
        return configuration;
    }
}
